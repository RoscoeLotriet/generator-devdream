var path = require('path');
var webpack = require('webpack');

module.exports = {
  devtool: 'eval',
    entry: [
    'webpack-hot-middleware/client?path=http://localhost:<%= webpackPort %>/__webpack_hmr',
    './src/index'
  ],
  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'bundle.js',
    publicPath: 'http://localhost:<%= webpackPort %>/dist/'
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin()    
  ],
  resolve: {
    alias: {
        rx: 'rx/dist/rx.lite.js',
    },
  },
  module: {
    loaders: [{
      test: /\.js$/,
      loader: 'babel',
      include: [path.join(__dirname, 'src')],
      query: {
                stage: 0,
                compact: false,
                plugins: ['react-transform'],
                extra: {
                "react-transform": {
                  "transforms": [{
                    "transform": "react-transform-hmr",
                    "imports": ["react"],
                    "locals": ["module"]
                  }, {
                      "transform": "react-transform-catch-errors",
                      "imports": ["react", "redbox-react"]
                  }]
                  }
                }
      }
    },
    {
      test: /\.css$/,
      loaders: ['style-loader', 'css-loader']
    },
    {
      test: /\.json$/,
      loader: 'json-loader'
    },
    {
      test: /\.less$/,
      loaders: ['style-loader', 'css-loader', 'less-loader'],
    },
    {
      test: /\.woff\d?(\?.+)?$/,
      loader: 'url-loader?limit=10000&minetype=application/font-woff',
    },
    {
      test: /\.ttf(\?.+)?$/,
      loader: 'url-loader?limit=10000&minetype=application/octet-stream',
    },
    {
      test: /\.eot(\?.+)?$/,
      loader: 'file-loader',
    },
    {
      test: /\.svg(\?.+)?$/,
      loader: 'url-loader?limit=10000&minetype=image/svg+xml',
    },
    {
      test: /\.png$/,
      loader: 'url-loader?limit=10000&mimetype=image/png',
    },
    {
      test: require.resolve('devdream-material-design-lite'),
      loader: 'exports?componentHandler'
    }]
  },
  node: {
    fs: 'empty',
    net: 'empty',
    tls: 'empty'
  }
};
