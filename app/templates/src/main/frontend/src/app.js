import React from 'react'
import ReactRouter from 'react-router'
import {RouteHandler} from 'react-router';
import routes from './routes.js'

const App = React.createClass({
    render() {
        return <RouteHandler {...this.props}/>;
    },
});

const router = ReactRouter.create({routes: getRoutes(routes), locations: ReactRouter.HistoryLocation});

if (module.hot) {
  module.hot.accept('./routes', function () {
    let routes = require('./routes.js');
    router.replaceRoutes(routes);
  });
}

function getRoutes(routes){
	return  (
	    <ReactRouter.Route name="app" path="/" handler={App}>
	        {routes}
	    </ReactRouter.Route>
	);
}

export default class Bootstrapper {

	start() {
    	router.run(function(Handler,state){
        var params = state.params;
    		React.render(<Handler params={params}/>, document.getElementById('root'));
    	});
    }
}