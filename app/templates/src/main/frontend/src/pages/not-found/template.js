import React from 'react'
import {CommonLayout} from 'devdream-ui-common'
import {
    NavGroup,
    NavLink
} from 'devdream-ui-core'

const render = function() {

    return (

        <CommonLayout fixedHeader pageTitle="404 - This page was not found" drawerTitle="404">

        </CommonLayout>

    );
};

export default render;
