import {ServiceDispatcher} from 'devdream-service-dispatcher'
const dispatcher = new ServiceDispatcher({serviceName: '<%= appName %>'})
import _ from 'underscore'

export default class Store {

	constructor(){

	// this.tasks = [];
	// this.processes = [];


	//	dispatcher.getQueryResponseTopic('tasks').subscribe((data) => {
    //		this.tasks = data;
    //	})

    //	dispatcher.getQueryResponseTopic('processes').subscribe((data) => {
    //		this.processes = data;
    //	})
    	
	}

	createSubscription(topic, handler){
		return dispatcher.getQueryResponseTopic(topic).subscribe(handler)
	}

	getState(){
		return { 
			pageObject: { 
				// processTaskMenuObjects : this.getProcessTaskMenuObjects(),
				// taskPanelObjects : this.getAllTasks()
			}
		};
	};

	command(commandType, commandProperties){
		dispatcher.dispatchCommand(commandType, commandProperties)
	}

	query(queryType, queryProperties){
		dispatcher.dispatchQuery(queryType, queryProperties);
	}

	// getAllTasks(){
	// 	return this.tasks;
	// }

	// getAllProcesses(){
	// 	return this.processes;
	// }

	// findTaskById(taskId){
	// 	return this.tasks.filter((task)=>{
	// 		return task.taskId == taskId
	// 	})[0];
	// }

	// findTaskByTaskDefinition(taskDefinitionId){
	// 	return this.tasks.filter((task)=>{
	// 		return task.taskDefinitionId == taskDefinitionId
	// 	})[0];
	// }

	// findProcessById(processId){
	// 	return this.processes.filter((process)=>{
	// 		return process.processId == processId
	// 	})[0];
	// }

	// getProcessTaskMenuObjects(){

	// 	const processes = _.map(this.processes, (process) => {

	// 		let taskCountMap = _.countBy(process.tasks, 'taskDefinitionId')
	// 		let taskGroupByDefinitionId = _.groupBy(process.tasks, 'taskDefinitionId')

	// 		const tasks = _.map(_.keys(taskCountMap), (taskDefinitionId) => {

	// 			const task = this.findTaskByTaskDefinition(taskDefinitionId);
				
	// 			if(!task){
	// 				return {};
	// 			}

	// 			return {
	// 				taskDefinitionId: task.taskDefinitionId,
	// 				taskName: task.taskName,
	// 				taskCount: taskCountMap[task.taskDefinitionId]
	// 			} 

	// 		})

	// 		return {
	// 			processId: process.processId,
	// 			processName: process.processName,
	// 			processTaskCount: process.tasks.length,
	// 			tasks: tasks
	// 		}

	// 	})

	// 	return processes;

	// }

}