import React from 'react'
import {CommonLayout} from 'devdream-ui-common'
import {
    NavGroup,
    NavLink,
    Badge,
    Card,
    CardTitle,
    CardTitleText,
    CardSupportingText,
    CardActions,
    LayoutSpacer,
    TextField,
    Checkbox,
    Button
} from 'devdream-ui-core'

class Template extends React.Component {

    render(){
    
    	const navGroups = ([
            <NavGroup>
                <NavLink>
                    <Badge value={99}>
                        <b>A menu badge</b>
                    </Badge>
                </NavLink>
            </NavGroup>
        ]);

        return (

            <CommonLayout fixedHeader fixedDrawer pageTitle="<%= appName %>" drawerTitle="<%= appName %>" navGroups={navGroups}>
                <Card className="mdl-shadow--4dp command-card">
                        <CardTitle className="mdl-card--expand">
                          <CardTitleText>Sign Up&nbsp;<i className="material-icons">face</i></CardTitleText>
                        </CardTitle>
                        <CardSupportingText>
                            <TextField label="Firstname" floatingLabel/>
                            <TextField label="Surname" floatingLabel/>
                            <TextField label="Profession" floatingLabel/>
                            <TextField label="Email" floatingLabel/>
                            <Checkbox inputId="my-checkbox" ripple label="Subscribe to our newsletter?"/>
                        </CardSupportingText>
                        <CardActions className="mdl-card--border">
                          <Button colored ripple raised accent>Sign Up</Button>
                        </CardActions>
                    </Card>
            </CommonLayout>

        );

    }
};

export default Template;
