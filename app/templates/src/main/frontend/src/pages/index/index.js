import React from 'react'
import {DefaultRoute} from 'react-router'
import Template from './template'
import Store from './store'
const PageStore = new Store()
// only load style when using webpack
/* istanbul ignore if  */
require('./style.less');


class IndexPage extends React.Component {

    constructor(){
        super();
        this.state = {};
        this.state.pageObject = {};
    }

	getStateFromStores(){
		return PageStore.getState()
	}

    componentDidMount(){

    //   	const sub = PageStore.createSubscription('processes', (data) => {
    //   		this.setState(this.getStateFromStores());
    //   	})

    //   	const sub1 = PageStore.createSubscription('tasks', (data) => {
    //   		this.setState(this.getStateFromStores());
    //   	})

    // PageStore.query('processes', {userId: 'roscoe'});
    //   	PageStore.query('tasks', {userId: 'roscoe'});

    	
    }

    render(){

    	return <Template pageObject={this.state.pageObject}/>;
    }

}

const IndexRoute = React.createElement(DefaultRoute, {name: 'home', key: 'route_default', handler: IndexPage});

export default IndexRoute;
