package com.devdream.<%= formattedClassImport %>;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;

import com.devdream.service.dispatcher.QueryPayloadConversionStrategy;

@SpringBootApplication
@EnableDiscoveryClient
public class ServiceApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(ServiceApplication.class, args);
	}

	@Bean
	public QueryPayloadConversionStrategy payloadConversionStrategy() {
		return new QueryPayloadConversionStrategy();
	}

}
