package com.devdream.<%= formattedClassImport %>;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@Profile("default")
public class WebpackProxy {

	@RequestMapping(value = { "/" })
	void home(HttpServletRequest request, HttpServletResponse response) throws MalformedURLException, URISyntaxException, IOException {
		proxy(request, response, "text/html");
	}

	@RequestMapping(value = { "/dist/bundle.js" })
	void dist(HttpServletRequest request, HttpServletResponse response) throws MalformedURLException, URISyntaxException, IOException {
		proxy(request, response, "text/javascript");
	}

	@RequestMapping(value = { "/dist/*.json" })
	void hotUpdate(HttpServletRequest request, HttpServletResponse response) throws MalformedURLException, URISyntaxException, IOException {
		proxy(request, response, "text/json");
	}

	private void proxy(HttpServletRequest request, HttpServletResponse response, String mimeType) throws URISyntaxException, MalformedURLException, IOException {
		URI uri = new URI("http", null, "localhost", <%= webpackPort %>, request.getRequestURI(), request.getQueryString(), null);
		response.setHeader("Content-Type", mimeType);

		System.out.println("Proxied to url -> " + uri.toURL().toString());

		IOUtils.copy(uri.toURL().openStream(), response.getOutputStream());

	}
}
