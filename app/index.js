'use strict';
var yeoman = require('yeoman-generator');
var chalk = require('chalk');
var yosay = require('yosay');
var _ = require('lodash');

module.exports = yeoman.generators.Base.extend({
  initializing: function () {
    
  },

  prompting: function () {
    var done = this.async();

    // Have Yeoman greet the user.
    this.log(yosay('Welcome to the ' + chalk.red('DevDream App') + ' generator!'));

    var prompts = [
	  	{
	    type: 'input',
	    name: 'appName',
	    message: 'Your app name',
	    default: this.appname,
	  	},
	  	{
	      type: 'input',
	      name: 'description',
	      message: 'Provide a description of your app',
	      default: 'No description yet'
	  	},
	  	{
	      type: 'input',
	      name: 'servicePort',
	      message: 'Provide a port for your service',
	      default: '8000'
	  	},
	  	{
	      type: 'input',
	      name: 'webpackPort',
	      message: 'Provide a port for webpack dev server',
	      default: '8001'
	  	},
	  	{
	      type: 'list',
	      name: 'style',
	      message: 'Do you want to use less, scss, styl or plain css?',
	      choices: ['less', 'scss', 'styl', 'css'],
	      default: 'less',
	      store: true // save for future
	    }
	];

    this.prompt(prompts, function (props) {
      this.options = props;
      done();
    }.bind(this));
  },

  writing: {
    app: function () {

    	this.props = {
    		appName: this.options.appName,
    		formattedAppPath: this.options.appName.replace('-', '/'),
    		formattedClassImport: this.options.appName.replace('-', '.'),
    		description: this.options.description,
    		servicePort: this.options.servicePort,
    		webpackPort: this.options.webpackPort,
    		style: this.options.style
    	}

    	this.log(yosay(
	      'I\'m busy creating your ' + chalk.cyan(this.options.appName) + ' app'
	    ));

	    this.fs.copy(
		    this.templatePath('src/main/frontend/.babelrc'),
		    this.destinationPath('src/main/frontend/.babelrc')
	    );

	    this.fs.copyTpl(
			this.templatePath('src/main/frontend/index.html'),
			this.destinationPath('src/main/frontend/index.html'),
			this.props
		);

		this.fs.copyTpl(
			this.templatePath('src/main/frontend/package.json'),
			this.destinationPath('src/main/frontend/package.json'),
			this.props
		);

		this.fs.copyTpl(
			this.templatePath('src/main/frontend/server.js'),
			this.destinationPath('src/main/frontend/server.js'),
			this.props
		);

		this.fs.copyTpl(
			this.templatePath('src/main/frontend/webpack.config.dev.js'),
			this.destinationPath('src/main/frontend/webpack.config.dev.js'),
			this.props
		);

		this.fs.copyTpl(
			this.templatePath('src/main/frontend/src'),
			this.destinationPath('src/main/frontend/src'),
			this.props
		);

		this.fs.copyTpl(
			this.templatePath('src/main/java/com/devdream/app'),
			this.destinationPath('src/main/java/com/devdream/' + this.props.formattedAppPath),
			this.props
		);

		this.fs.copyTpl(
			this.templatePath('src/main/resources/application.yml'),
			this.destinationPath('src/main/resources/application.yml'),
			this.props
		);

		this.fs.copyTpl(
			this.templatePath('src/main/resources/bootstrap.yml'),
			this.destinationPath('src/main/resources/bootstrap.yml'),
			this.props
		);

		this.fs.copyTpl(
			this.templatePath('build.gradle'),
			this.destinationPath('build.gradle'),
			this.props
		);

		this.fs.copyTpl(
			this.templatePath('manifest.yml'),
			this.destinationPath('manifest.yml'),
			this.props
		);
    }
  },

  install: function () {
    this.spawnCommand("npm", ["install"], { cwd: 'src/main/frontend'})
  }
});