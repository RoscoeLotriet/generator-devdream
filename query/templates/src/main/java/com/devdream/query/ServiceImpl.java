package com.devdream.<%= appName %>.query.<%= lowerCaseQueryName %>;

import org.springframework.stereotype.Service;

import com.devdream.service.dispatcher.Query;
import com.devdream.service.dispatcher.Response;
import com.devdream.service.dispatcher.ResponseBuilder;

@Service
public class <%= capitalizedQueryName %>QueryServiceImpl implements <%= capitalizedQueryName %>QueryService {

	@Override
   	public Response query<%= capitalizedQueryName %>(Query <%= camelCaseQueryName %>Query){
   		//TODO: Implement query logic here
   		return new ResponseBuilder().fromQuery(<%= camelCaseQueryName %>Query).build();
   	}

}
