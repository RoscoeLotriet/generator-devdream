package com.devdream.<%= appName %>.query.<%= lowerCaseQueryName %>;

import com.devdream.service.dispatcher.Query;
import com.devdream.service.dispatcher.Response;

public interface <%= capitalizedQueryName %>QueryService {
   Response query<%= capitalizedQueryName %>(Query <%= camelCaseQueryName %>Query);
}
