package com.devdream.<%= appName %>.query.<%= lowerCaseQueryName %>;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.devdream.service.dispatcher.Query;
import com.devdream.service.dispatcher.Response;

@RestController
public class <%= capitalizedQueryName %>QueryController {

	<%= capitalizedQueryName %>QueryService service;

	@Autowired
	public <%= capitalizedQueryName %>QueryController(<%= capitalizedQueryName %>QueryService service) {
		this.service = service;
	}

	@RequestMapping(value = "/<%= kebabCaseQueryName %>", method = RequestMethod.GET)
	public ResponseEntity<Response> <%= camelCaseQueryName %>Query(Query <%= camelCaseQueryName %>Query) {
		return new ResponseEntity<Response>(service.query<%= capitalizedQueryName %>(<%= camelCaseQueryName %>Query), HttpStatus.OK);
	}

}
