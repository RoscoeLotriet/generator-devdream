'use strict';
var yeoman = require('yeoman-generator');
var chalk = require('chalk');
var yosay = require('yosay');
var _ = require('lodash');

module.exports = yeoman.generators.Base.extend({
  initializing: function () {
    
  },

  prompting: function () {
    var done = this.async();

    // Have Yeoman greet the user.
    this.log(yosay('Welcome to the ' + chalk.red('DevDream Query') + ' generator!'));

    var prompts = [
	  	{
	    type: 'input',
	    name: 'queryName',
	    message: 'Your query name',
	  	}	  	
	];

    this.prompt(prompts, function (options) {
      this.options = options;
      done();
    }.bind(this));
  },

  writing: {
    app: function () {

    	this.appName = this.appname.replace(' ', '-')

    	this.props = {
    		appName: this.appName.replace('-', '.'),
    		appFolder: this.appName.replace('-', '/'),
    		queryName: this.options.queryName,
    		capitalizedQueryName: _.capitalize(_.camelCase(this.options.queryName)),
    		kebabCaseQueryName: _.kebabCase(this.options.queryName),
    		camelCaseQueryName: _.camelCase(this.options.queryName),
    		lowerCaseQueryName: this.options.queryName.toLowerCase().replace('-', '')
    	}

    	this.log(yosay(
	      'I\'m busy creating your ' + chalk.cyan(this.props.capitalizedQueryName) + ' query'
	    ));

	    this.fs.copyTpl(
		    this.templatePath('src/main/java/com/devdream/query/Service.java'),
		    this.destinationPath('src/main/java/com/devdream/'+this.props.appFolder+'/query/'+this.props.lowerCaseQueryName +'/'+ this.props.capitalizedQueryName+'QueryService.java'),
		    this.props
	    );

	    this.fs.copyTpl(
		    this.templatePath('src/main/java/com/devdream/query/ServiceImpl.java'),
		    this.destinationPath('src/main/java/com/devdream/'+this.props.appFolder+'/query/'+this.props.lowerCaseQueryName +'/'+ this.props.capitalizedQueryName+'QueryServiceImpl.java'),
		    this.props
	    );

	    this.fs.copyTpl(
		    this.templatePath('src/main/java/com/devdream/query/Controller.java'),
		    this.destinationPath('src/main/java/com/devdream/'+this.props.appFolder+'/query/'+this.props.lowerCaseQueryName +'/'+ this.props.capitalizedQueryName+'QueryController.java'),
		    this.props
	    );
	    	   
    }
  }
});