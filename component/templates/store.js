import {ServiceDispatcher} from 'devdream-service-dispatcher'
const dispatcher = new ServiceDispatcher({serviceName: '<%= appName %>'})
import _ from 'underscore'

export default class Store {

	constructor(){

	}

	registerOnChange(handler){
		this.OnChangeHandler = handler;
	}

	createQuerySubscription(topic, handler){
		return dispatcher.getQueryResponseTopic(topic).subscribe(handler)
	}

	createCommandSubscription(topic, handler){
		return dispatcher.getCommandResponseTopic(topic).subscribe(handler)
	}

	getState(){
		return { 
			pageObject: { 

			}
		};
	};

	defaultCommandProperties(commandProperties){
		this.commandProperties = commandProperties;
	}

	command(serviceId, commandType, commandProperties){
		const commandPayload = Object.assign(this.commandProperties, commandProperties);
		dispatcher.dispatchCommand(serviceId, commandType, commandPayload)
	}

	query(serviceId, queryType, queryProperties){
		dispatcher.dispatchQuery(serviceId, queryType, queryProperties);
	}

}