import React from 'react'
import Template from './template'
import Store from './store'
const PageStore = new Store()
// only load style when using webpack
/* istanbul ignore if  */
require('./style.less');


class IndexComponent extends React.Component {

    constructor(){
        super();
        this.state = {};
        this.state.pageObject = {};
    }

	getStateFromStores(){
		return PageStore.getState()
	}

    componentDidMount(){
        PageStore.registerOnChange(()=>{this.setState(this.getStateFromStores)})
    }

    render(){

    	return <Template pageObject={this.state.pageObject} valueLinkGroup={this.props.valueLinkGroup}/>;
    }

}

export default IndexComponent;
