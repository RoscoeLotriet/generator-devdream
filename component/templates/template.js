import React from 'react'
import {
    Badge,
    Button,
    Card,
    CardTitle,
    CardTitleText,
    CardSupportingText,
    CardActions,
    Cell,
    Checkbox,
    CommandValueLink,
    Grid,
    LayoutSpacer,
    NavGroup,
    NavLink,
    Select,
    TextField,
    ValueLink
} from 'devdream-ui-core'

class Template extends React.Component {

    constructor(){
        super()
        this.state = {}
    }

    render(){
        
        const valueLinkGroup = this.props.valueLinkGroup;

        return (

            
                <Card className="mdl-shadow--4dp <%= kebabCaseName %>">
                        <CardTitle className="mdl-card--expand">
                          <CardTitleText><%= capitalizedCamelCaseName %>&nbsp;<i className="material-icons">face</i></CardTitleText>
                        </CardTitle>
                        <CardSupportingText>
                            <TextField label="Firstname" floatingLabel valueLink={ValueLink(this, 'firstname')}/>
                            <TextField label="Surname" floatingLabel valueLink={ValueLink(this, 'surname')}/>
                            <TextField label="Profession" floatingLabel valueLink={ValueLink(this, 'profession')}/>
                            <TextField label="Email" floatingLabel valueLink={ValueLink(this, 'email')}/>
                            <Checkbox inputId="my-checkbox" ripple label="Subscribe to our newsletter?" checkedLink={ValueLink(this, 'subscribe')}/>
                        </CardSupportingText>
                        <CardActions className="mdl-card--border">
                          <Button colored ripple raised accent><%= capitalizedCamelCaseName %></Button>
                        </CardActions>
                    </Card>
           

        );

    }
};

export default Template;
