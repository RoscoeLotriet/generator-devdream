'use strict';
var yeoman = require('yeoman-generator');
var chalk = require('chalk');
var yosay = require('yosay');
var _ = require('lodash');

module.exports = yeoman.generators.Base.extend({
  initializing: function () {
    
  },

  prompting: function () {
    var done = this.async();

    this.argument('name', {
      required: true,
      type: String,
      desc: 'The component name'
    });

    // Have Yeoman greet the user.
    this.log(yosay('Welcome to the ' + chalk.red('DevDream App') + ' component generator!'));

    done();
 
  },

  writing: {
    app: function () {

    	this.props = {
    	  appName: this.appname,
	      camelcaseName: _.camelCase(this.name),
	      capitalizedCamelCaseName: _.capitalize(_.camelCase(this.name)),
	      capitalizedName: _.capitalize(this.name),
	      kebabCaseName: _.kebabCase(this.name)
    	}

    	this.log(yosay(
	      'I\'m busy creating your ' + this.name + ' ' + chalk.cyan('component')
	    ));

		this.fs.copyTpl(
			this.templatePath('index.js'),
			this.destinationPath('src/main/frontend/src/components/' + this.name + '/index.js'),
			this.props
		);

		this.fs.copyTpl(
			this.templatePath('store.js'),
			this.destinationPath('src/main/frontend/src/components/' + this.name + '/store.js'),
			this.props
		);

		this.fs.copyTpl(
			this.templatePath('style.less'),
			this.destinationPath('src/main/frontend/src/components/' + this.name + '/style.less'),
			this.props
		);

		this.fs.copyTpl(
			this.templatePath('template.js'),
			this.destinationPath('src/main/frontend/src/components/' + this.name + '/template.js'),
			this.props
		);
   
    }
  }

});