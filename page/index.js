'use strict';
var yeoman = require('yeoman-generator');
var chalk = require('chalk');
var yosay = require('yosay');
var _ = require('lodash');

module.exports = yeoman.generators.Base.extend({
  initializing: function () {
    
  },

  prompting: function () {
    var done = this.async();

    this.argument('name', {
      required: true,
      type: String,
      desc: 'The page name'
    });

    // Have Yeoman greet the user.
    this.log(yosay('Welcome to the ' + chalk.red('DevDream App') + ' page generator!'));

    done();
 
  },

  writing: {
    app: function () {

    	this.props = {
    	  appName: this.appname,
	      camelcaseName: _.camelCase(this.name),
	      capitalizedCamelCaseName: _.capitalize(_.camelCase(this.name)),
	      capitalizedName: _.capitalize(this.name),
	      kebabCaseName: _.kebabCase(this.name)
    	}

    	this.log(yosay(
	      'I\'m busy creating your ' + this.name + ' ' + chalk.cyan('page')
	    ));

		this.fs.copyTpl(
			this.templatePath('index.js'),
			this.destinationPath('src/main/frontend/src/pages/' + this.name + '/index.js'),
			this.props
		);

		this.fs.copyTpl(
			this.templatePath('store.js'),
			this.destinationPath('src/main/frontend/src/pages/' + this.name + '/store.js'),
			this.props
		);

		this.fs.copyTpl(
			this.templatePath('style.less'),
			this.destinationPath('src/main/frontend/src/pages/' + this.name + '/style.less'),
			this.props
		);

		this.fs.copyTpl(
			this.templatePath('template.js'),
			this.destinationPath('src/main/frontend/src/pages/' + this.name + '/template.js'),
			this.props
		);

		// update routes
	    var path = this.destinationPath('src/main/frontend/src/routes.js');
	    var processProps = this.props;

	    this.fs.copy(path, path, {
	      process: function (content) {
	        // add new import
	        var re = /import(.+?);/;
	        var newString = 'import ' + processProps.capitalizedCamelCaseName;
	        newString += ' from \'./pages/' + processProps.kebabCaseName + '/index\';\nimport$1;';
	        var newContent = content.toString().replace(re, newString);
	        // add new route to array
	        newString = 'const routes = [\n    ' + processProps.capitalizedCamelCaseName + ',';
	        re = /const routes = \[/;
	        newContent = newContent.replace(re, newString);
	        // return new file
	        return newContent;
	      }
	    });

	   
    }
  }

});