import React from 'react'
import {CommonLayout} from 'devdream-ui-common'
import {
    NavGroup,
    NavLink,
    Badge,
    Card,
    CardTitle,
    CardTitleText,
    CardSupportingText,
    CardActions,
    LayoutSpacer,
    TextField,
    Checkbox,
    Button,
    ValueLink
} from 'devdream-ui-core'

class Template extends React.Component {

    constructor(){
        super()
        this.state = {}
    }

    render(){
        
    	const navGroups = ([
            <NavGroup>
                <NavLink>
                    <Badge value={99}>
                        <b>A menu badge</b>
                    </Badge>
                </NavLink>
            </NavGroup>
        ]);

        return (

            <CommonLayout fixedHeader fixedDrawer pageTitle="<%= capitalizedCamelCaseName %>" drawerTitle="<%= capitalizedCamelCaseName %>" navGroups={navGroups}>
                <Card className="mdl-shadow--4dp <%= kebabCaseName %>">
                        <CardTitle className="mdl-card--expand">
                          <CardTitleText><%= capitalizedCamelCaseName %>&nbsp;<i className="material-icons">face</i></CardTitleText>
                        </CardTitle>
                        <CardSupportingText>
                            <TextField label="Firstname" floatingLabel valueLink={ValueLink(this, 'firstname')}/>
                            <TextField label="Surname" floatingLabel valueLink={ValueLink(this, 'surname')}/>
                            <TextField label="Profession" floatingLabel valueLink={ValueLink(this, 'profession')}/>
                            <TextField label="Email" floatingLabel valueLink={ValueLink(this, 'email')}/>
                            <Checkbox inputId="my-checkbox" ripple label="Subscribe to our newsletter?" checkedLink={ValueLink(this, 'subscribe')}/>
                        </CardSupportingText>
                        <CardActions className="mdl-card--border">
                          <Button colored ripple raised accent><%= capitalizedCamelCaseName %></Button>
                        </CardActions>
                    </Card>
            </CommonLayout>

        );

    }
};

export default Template;
