'use strict';
var yeoman = require('yeoman-generator');
var chalk = require('chalk');
var yosay = require('yosay');
var _ = require('lodash');

module.exports = yeoman.generators.Base.extend({
  initializing: function () {
    
  },

  prompting: function () {
    var done = this.async();

    // Have Yeoman greet the user.
    this.log(yosay('Welcome to the ' + chalk.red('DevDream Aggregate') + ' generator!'));

    var prompts = [
	  	{
	    type: 'input',
	    name: 'aggregateName',
	    message: 'Your aggregate name',
	  	}	  	
	];

    var commandEvents = [];

    var commandEventPrompting = function(self){

    	var commandEventPrompts = [
    		{
	    		type: 'input',
	    		name: 'command',
	    		message: 'Provide a command type Eg. register-client (leave blank to continue)'
	    	},{
	    		type: 'input',
			    name: 'event',
			    message: 'Provide an associated event type Eg. client-registered',
			    when: function(options){return options.command !== '';}
	    	}
	    ];

    	self.prompt(commandEventPrompts, function(options){
    	if(options.command !== ''){
          commandEvents.push(
          	{
          		command: options.command,
          		camelCaseCommand: _.camelCase(options.command),
          		kebabCaseCommand: _.kebabCase(options.command),
          		capitalizedCamelCaseCommand: _.capitalize(_.camelCase(options.command)),
          		event: options.event,
          		camelCaseEvent: _.camelCase(options.event),
          		kebabCaseEvent: _.kebabCase(options.event),
          		capitalizedCamelCaseEvent: _.capitalize(_.camelCase(options.event)),
          	}
          );
          commandEventPrompting(self);
        }else{
          self.commandEvents = commandEvents;
          done();
        }
    	});
    }

    this.prompt(prompts, function (options) {
      this.options = options;
      commandEventPrompting(this);
    }.bind(this));
  },

  writing: {
    app: function () {

      this.appName = this.appname.replace(' ', '-')

      this.props = {
        appName: this.appName.replace('-', '.'),
        appFolder: this.appName.replace('-', '/'),
    		aggregateName: this.options.aggregateName,
    		commandEvents: this.commandEvents,
    		capitalizedAggregateName: _.capitalize(this.options.aggregateName),
    		kebabCaseAggregateName: _.kebabCase(this.options.aggregateName),
    		lowerCaseAggregateName: this.options.aggregateName.toLowerCase()
    	}

    	this.log(yosay(
	      'I\'m busy creating your ' + chalk.cyan(this.props.capitalizedAggregateName) + ' aggregate'
	    ));

	    this.fs.copyTpl(
		    this.templatePath('src/main/java/com/devdream/aggregate/Aggregate.java'),
		    this.destinationPath('src/main/java/com/devdream/'+this.props.appFolder+'/aggregate/'+this.props.lowerCaseAggregateName +'/'+ this.props.capitalizedAggregateName+'.java'),
		    this.props
	    );

	    this.fs.copyTpl(
		    this.templatePath('src/main/java/com/devdream/eventsubscriber/EventSubscriber.java'),
		    this.destinationPath('src/main/java/com/devdream/'+this.props.appFolder+'/eventsubscriber/'+this.props.lowerCaseAggregateName +'/'+ this.props.capitalizedAggregateName+'EventSubscriber.java'),
		    this.props
	    );

	    this.fs.copyTpl(
		    this.templatePath('src/main/java/com/devdream/aggregate/Service.java'),
		    this.destinationPath('src/main/java/com/devdream/'+this.props.appFolder+'/aggregate/'+this.props.lowerCaseAggregateName +'/'+ this.props.capitalizedAggregateName+'Service.java'),
		    this.props
	    );

	    this.fs.copyTpl(
		    this.templatePath('src/main/java/com/devdream/aggregate/ServiceImpl.java'),
		    this.destinationPath('src/main/java/com/devdream/'+this.props.appFolder+'/aggregate/'+this.props.lowerCaseAggregateName +'/'+ this.props.capitalizedAggregateName+'ServiceImpl.java'),
		    this.props
	    );

	    this.fs.copyTpl(
		    this.templatePath('src/main/java/com/devdream/aggregate/Controller.java'),
		    this.destinationPath('src/main/java/com/devdream/'+this.props.appFolder+'/aggregate/'+this.props.lowerCaseAggregateName +'/'+ this.props.capitalizedAggregateName+'CommandController.java'),
		    this.props
	    );
	   
    }
  }
});