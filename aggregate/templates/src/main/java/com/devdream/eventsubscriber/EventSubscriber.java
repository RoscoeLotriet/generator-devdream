package com.devdream.<%= appName %>.eventsubscriber.<%= lowerCaseAggregateName %>;

import com.devdream.eventstore.domain.Event;
import com.devdream.eventstore.eventlistener.annotations.EventListener;
import com.devdream.eventstore.eventlistener.annotations.EventSubscriber;

@EventSubscriber
public class <%= capitalizedAggregateName %>EventSubscriber {

	<% commandEvents.forEach(function(commandEvent){ %>
	@EventListener("<%= commandEvent.kebabCaseEvent %>")
	public void <%= commandEvent.camelCaseEvent %>(Event <%= commandEvent.camelCaseEvent %>) {
		//TODO: Perform event listener logic here, create read/query models or dispatch downstream commands
	}
    <% }) %>
}
