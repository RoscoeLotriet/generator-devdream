package com.devdream.<%= appName %>.aggregate.<%= lowerCaseAggregateName %>;

import com.devdream.eventstore.domain.Command;

public interface <%= capitalizedAggregateName %>Service {

    <% commandEvents.forEach(function(commandEvent){ %>
	void dispatch<%= commandEvent.capitalizedCamelCaseCommand %>(Command <%= commandEvent.camelCaseCommand %>);

    <% }) %>
}
