package com.devdream.<%= appName %>.aggregate.<%= lowerCaseAggregateName %>;

import java.util.List;

import com.devdream.eventstore.commandhandling.annotations.CommandHandler;
import com.devdream.eventstore.domain.Command;
import com.devdream.eventstore.domain.Event;
import com.devdream.eventstore.domain.annotations.Aggregate;
import com.devdream.eventstore.eventhandling.annotations.EventHandler;
import com.devdream.eventstore.util.EventBuilder;
import com.devdream.eventstore.util.EventUtil;

@Aggregate
public class <%= capitalizedAggregateName %> {

    <% commandEvents.forEach(function(commandEvent){ %>
    	@CommandHandler("<%= commandEvent.kebabCaseCommand %>")
		public List<Event> <%= commandEvent.camelCaseCommand %>(Command <%= commandEvent.camelCaseCommand %>) {
			//TODO: Perform command handling logic & invariant checks here
			return EventUtil.events(new EventBuilder("<%= commandEvent.kebabCaseEvent %>").fromCommand(<%= commandEvent.camelCaseCommand %>).build());
		}

		@EventHandler("<%= commandEvent.kebabCaseEvent %>")
		public void <%= commandEvent.camelCaseEvent %>(Event <%= commandEvent.camelCaseEvent %>) {
			//TODO: Perform event handling logic here, primarily to set aggregate state
		}
    <% }) %>
}
