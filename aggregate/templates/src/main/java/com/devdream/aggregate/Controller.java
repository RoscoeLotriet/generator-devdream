package com.devdream.<%= appName %>.aggregate.<%= lowerCaseAggregateName %>;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.devdream.eventstore.util.CommandBuilder;
import com.devdream.service.dispatcher.Command;

@RestController
public class <%= capitalizedAggregateName %>CommandController {

	<%= capitalizedAggregateName %>Service service;

	@Autowired
	public <%= capitalizedAggregateName %>CommandController(<%= capitalizedAggregateName %>Service service) {
		this.service = service;
	}
	<% commandEvents.forEach(function(commandEvent){ %>
	@RequestMapping(value = "/<%= commandEvent.kebabCaseCommand %>", method = RequestMethod.POST)
	@SuppressWarnings({ "rawtypes" })
	public ResponseEntity <%= commandEvent.camelCaseCommand %>(@RequestBody Command <%= commandEvent.camelCaseCommand %>Command) {
		service.dispatch<%= commandEvent.capitalizedCamelCaseCommand %>(new CommandBuilder().fromCommand(<%= commandEvent.camelCaseCommand %>Command).build());
		return new ResponseEntity(HttpStatus.OK);
	}
    <% }) %>
}
