package com.devdream.<%= appName %>.aggregate.<%= lowerCaseAggregateName %>;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devdream.eventstore.commandhandling.CommandBus;
import com.devdream.eventstore.domain.Command;

@Service
public class <%= capitalizedAggregateName %>ServiceImpl implements <%= capitalizedAggregateName %>Service {

	CommandBus commandBus;

	@Autowired
	public <%= capitalizedAggregateName %>ServiceImpl(CommandBus commandBus){
		this.commandBus = commandBus;
	}

    <% commandEvents.forEach(function(commandEvent){ %>
	public void dispatch<%= commandEvent.capitalizedCamelCaseCommand %>(Command <%= commandEvent.camelCaseCommand %>){
		commandBus.dispatch(<%= commandEvent.camelCaseCommand %>);
	}

    <% }) %>

}
